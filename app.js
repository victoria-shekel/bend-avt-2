const http = require('http');

/**
 * MongoDB
 */
const os = require('os');
const userInfo = os.userInfo();
const uid = userInfo.uid;
const name = userInfo.username;

const hostnameMongo = '0.0.0.0';
const portMongo = 1000 + uid;

const MongoClient = require('mongodb').MongoClient;

const urlMongo = 'mongodb://127.0.0.1:27017/';
const mongoClient = new MongoClient(urlMongo);
let count = 0;

async function runMongo() {
    try {
        // Подключаемся к серверу
        await mongoClient.connect();
        // обращаемся к базе данных пользователя
        const db = mongoClient.db(`${name}`);
        // выполняем пинг для проверки подключения
        const result = await db.command({ ping: 1 });
        console.log('Подключение с сервером успешно установлено');
        const collection = db.collection('users');
        count = await collection.countDocuments();
        console.log(`В коллекции users ${count} документа/ов`);
        console.log(result);
    } catch(err) {
        console.log('Возникла ошибка');
        console.log(err);
    } finally {
        // Закрываем подключение при завершении работы или при ошибке
        await mongoClient.close();
        console.log("Подключение закрыто");
    }
}

runMongo().catch(console.error);

const serverMongo = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end(`Hello ${name}, you have ${count} documents` );
});
serverMongo.listen(portMongo, hostnameMongo, () => {
    console.log(`Server Mongo running at http://${hostnameMongo}:${port}/`);
});

/**
 * Базовый запрос
 */
const hostname = '127.0.0.1';
const portBasic = 3000;
const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World');
});
server.listen(portBasic, hostname, () => {
    console.log(`Server basic running at http://${hostname}:${portBasic}/`);
});

/**
 * Микросервис героев
 */
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const portHeroes = process.argv.slice(2)[0];
const app = express();

app.use(bodyParser.json());

const powers = [
    { id: 1, name: 'flying' },
    { id: 2, name: 'teleporting' },
    { id: 3, name: 'super strength' },
    { id: 4, name: 'clairvoyance'},
    { id: 5, name: 'mind reading' }
];

const heroes = [
    {
        id: 1,
        type: 'spider-dog',
        displayName: 'Cooper',
        powers: [1, 4],
        img: 'cooper.jpg',
        busy: false
    },
    {
        id: 2,
        type: 'flying-dogs',
        displayName: 'Jack &amp; Buddy',
        powers: [2, 5],
        img: 'jack_buddy.jpg',
        busy: false
    },
    {
        id: 3,
        type: 'dark-light-side',
        displayName: 'Max &amp; Charlie',
        powers: [3, 2],
        img: 'max_charlie.jpg',
        busy: false
    },
    {
        id: 4,
        type: 'captain-dog',
        displayName: 'Rocky',
        powers: [1, 5],
        img: 'rocky.jpg',
        busy: false
    }
];

app.get('/heroes', (req, res) => {
    console.log('Returning heroes list');
    res.send(heroes);
});

app.get('/powers', (req, res) => {
    console.log('Returning powers list');
    res.send(powers);
});

app.post('/hero/**', (req, res) => {
    const heroId = parseInt(req.params[0]);
    const foundHero = heroes.find(subject => subject.id === heroId);

    if (foundHero) {
        for (let attribute in foundHero) {
            if (req.body[attribute]) {
                foundHero[attribute] = req.body[attribute];
                console.log(`Set ${attribute} to ${req.body[attribute]} in hero: ${heroId}`);
            }
        }
        res.status(202).header({Location: `http://localhost:${portHeroes}/hero/${foundHero.id}`}).send(foundHero);
    } else {
        console.log(`Hero not found.`);
        res.status(404).send();
    }
});

app.use('/img', express.static(path.join(__dirname,'img')));

console.log(`Heroes service listening on port ${portHeroes}`);
app.listen(portHeroes);
